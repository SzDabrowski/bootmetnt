
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from  blogspider.spiders.articles import ArticlesSpider

process = CrawlerProcess(get_project_settings())
process.crawl(ArticlesSpider)
process.start()