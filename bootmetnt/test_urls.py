from django.urls import reverse, resolve
from django.test import TestCase

class TestUrls(TestCase):

    def test_stats_url(self):
        path = reverse('stats')
        assert resolve(path).view_name == 'stats'

    def test_authors_url(self):
        path = reverse('authors')
        assert resolve(path).view_name == 'authors'

    def test_authors_stats_url(self):
        path = reverse('authors_stats', kwargs={'author' : 'authorurl'})
        assert resolve(path).view_name == 'authors_stats'