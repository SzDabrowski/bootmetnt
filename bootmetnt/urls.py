from django.urls import path
from curls.views import AuthorStats, Authors, Stats

urlpatterns = [
    path('authors/', Authors.as_view(),  name ='authors'),
    path('stats/', Stats.as_view(), name='stats'),
    path('stats/<str:author>/', AuthorStats.as_view(), name='authors_stats')
]