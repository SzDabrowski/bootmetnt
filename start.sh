#!/usr/bin/env bash

python3 manage.py makemigrations
python3 manage.py migrate

python3 go-spider.py
python manage.py check
python3 manage.py runserver 0.0.0.0:8080