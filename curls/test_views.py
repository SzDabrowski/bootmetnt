from django.test import RequestFactory
from django.urls import reverse
from curls.models import Words
from curls import views
from django.test import TestCase
from mixer.backend.django import mixer

class TestViews(TestCase):

    def test_authors(self):
        path = reverse('authors')
        request = RequestFactory().get(path)
        request.words = mixer.blend(Words)

        response = views.Authors.as_view()(request)
        assert response.status_code == 200

    def test_stats(self):
        path = reverse('stats')
        request = RequestFactory().get(path)
        request.words = mixer.blend(Words)

        response = views.Stats.as_view()(request)
        assert response.status_code == 200

    def test_authorstats(self):
        path = reverse('authors_stats', kwargs={'author': 'testname'})
        request = RequestFactory().get(path)
        request.words = mixer.blend(Words)

        response = views.AuthorStats.as_view()(request,author = 'testname')
        assert response.status_code == 200

