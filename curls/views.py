from rest_framework import generics
from .serializers import AuthorStatsSerializer, AuthorSerializer, StatsSerializer
from .models import Words
from django.db.models import Count
from rest_framework.response import Response

class AuthorStats(generics.ListAPIView):
    #view for stats of every matching author
    serializer_class = AuthorStatsSerializer

    def get_queryset(self):
        author = self.kwargs['author']
        queryset = Words.objects.values('data').annotate(count=Count('data')).filter(authorurl=author)\
                   .order_by('-count')[:10]
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        return Response(queryset.values_list('data','count'))


class Authors(generics.ListAPIView):
    #view for list of authors with names and urls
    serializer_class = AuthorSerializer

    def get_queryset(self):
        queryset = Words.objects.values('authorname','authorurl').distinct()
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        return Response(queryset.values_list('authorname','authorurl'))


class Stats(generics.ListAPIView):
    #view for global stats
    serializer_class = StatsSerializer

    def get_queryset(self):
        queryset = Words.objects.values('data').annotate(count=Count('data')).order_by('-count')[:10]
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        return Response(queryset.values_list('data', 'count'))
