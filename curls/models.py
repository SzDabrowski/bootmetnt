from django.db import models
# Create your models here.

class Words(models.Model):
    #main table for storing every word in one record with its author name
    data = models.CharField(max_length=30,null=False)
    authorname = models.CharField(max_length=30, null=False)
    authorurl = models.CharField(max_length=30, null=False)

